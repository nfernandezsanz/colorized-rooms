# Colorized Rooms

A floor plan is an abstract representation of a building floor and defined by the distribution of walls and space.  For this project, floor plans are represented by ASCII characters, with walls being “#” and empty spaces being “ “ (white space). A room is defined by a cluster of adjacent spaces surrounded by walls and doors. A door is defined by a single space that divides two collinear walls.

Rooms are easily distinguishable from each other by using colors. 

## Considerations

* All spaces are reachable.
* There are no rooms 1 space wide (for example, narrow hallways).
* All rooms are surrounded by walls

## Getting started

The excersice was developed using [Python 3.8.10](https://www.python.org/downloads/release/python-3810/). To run the exercise Python and [PIP](https://pypi.org/project/pip/) **must** be installed.

## Virtual Enviroment

It's recommended to create a virtual environment to manage dependencies. Assuming you have Python 3 installed, follow these steps to create a virtual environment:

```
python3 -m venv venv
source venv/bin/activate
```

To deactivate the the virtual enviroment:

```
deactivate
```

## Install dependencies

```
pip install -r requirements.txt 
```

## Create floor plans

To start using the system, you need to create floor plans, its recommended to create them in the [examples](./examples/) directory. 

You will find many examples in the directory.

## Execute

```
python3 main.py
```

### Example execution
```
python3 main.py
Enter the directory path: examples/rectangle.txt
```

## Running tests

In order to execute the tests [Pytest](https://docs.pytest.org/en/7.4.x/) must be installed in the system or virtual env. 

Then execute:
```
pytets
```



## Author
* Nicolas Javier Fernandez Sanz 

* Email: nicolas@fernandezsanz.com

## Project status
This code was developed in a timebox of 45 minutes. There are many improvements to be done. 
