from models import FloorPlan
from colors import ColoredTextStrategy, DefaultColorStrategy

directory_path = input("Enter the directory path: ")
floor_plan = FloorPlan(directory_path)
floor_plan.print_color_floor_plan(ColoredTextStrategy())