class ColorStrategy:
    def get_color(self, value: int) -> str:
        raise NotImplementedError("Subclasses must implement this method.")
    
class DefaultColorStrategy(ColorStrategy):
    def get_color(self, value: int) -> str:
        if value == 0:
            return " "
        elif value == -1:
            return "/"
        else:
            return str(value)
        
class ColoredTextStrategy(ColorStrategy):
    def get_color(self, value: int) -> str:
        if value == 0:
            return "\033[48;5;255m  \033[m"   # White space for default/uncolored cells
        elif value == -1:
            return "\033[48;5;0m  \033[m"     # Black for doors
        else:
            color_code = 16 + (value * 10) % 216   # Use the 6x6x6 color cube for room colors
            return f"\033[48;5;{color_code}m  \033[m"