import numpy as np
from typing import List
from colors import ColorStrategy

class FloorPlan:
    DOOR = -1
    
    def __init__(self, filename: str, wall_char: str = "#", room_char: str = " "):
        self.filename = filename
        self.__wall_char = wall_char
        self.__room_char = room_char
        self.floor_plan = self.read_floor_plan()
        
    @property
    def wall_char(self) -> str:
        return self.__wall_char

    @wall_char.setter
    def wall_char(self, char: str):
        if len(char) != 1 or char.isnumeric():
            raise ValueError("Wall character must be a single non-numeric character.")
        self.wall_char = char

    @property
    def room_char(self) -> str:
        return self.__room_char

    @room_char.setter
    def room_char(self, char: str):
        if len(char) != 1 or char.isnumeric():
            raise ValueError("Room character must be a single non-numeric character.")
        self.__room_char = char
    
    def read_floor_plan(self) -> np.ndarray:
        # Read file (text file)
        try:
            with open(self.filename, 'r') as f:
                data = f.read().splitlines()
        except FileNotFoundError:
            raise FileNotFoundError(f"Error: The file:'{self.filename}' could not be found.")

        return self.generate_matrix(data)

    def generate_matrix(self, data : List) -> np.ndarray:
        max_len = max(len(row) for row in data)
        for i, row in enumerate(data):
            if len(row) < max_len:
                data[i] += self.wall_char * (max_len - len(row))
        return np.array([list(row) for row in data])

    def is_door(self, row : int, col: int):
        if self.floor_plan[row, col] != self.room_char:
            return False
        if row > 0 and row < self.floor_plan.shape[0] - 1:
            if self.floor_plan[row - 1, col] == self.wall_char and self.floor_plan[row + 1, col] == self.wall_char:
                return True
        if col > 0 and col < self.floor_plan.shape[1] - 1:
            if self.floor_plan[row, col - 1] == self.wall_char and self.floor_plan[row, col + 1] == self.wall_char:
                return True
        return False
    
    def get_neighboring_colors(self,color_plan : np.ndarray, row: int, col: int) -> int:
        # Get the blocks of the neighboards
        block = color_plan[max(row - 1, 0):min(row + 2, color_plan.shape[0]),
                        max(col - 1, 0):min(col + 2, color_plan.shape[1])]
        
        # Find the unique colors in the block.
        color = np.unique(block)
        
        return np.max(color)
    
    def color_floor_plan(self) -> np.ndarray:
        rows, cols = self.floor_plan.shape
        # Base array for colored result
        color_plan = np.zeros((rows, cols), dtype=int)
        current_color = 0

        for row in range(rows):
            for col in range(cols):
                
                # Check for doors
                if self.is_door(row, col):
                    color_plan[row, col] = self.DOOR
                # Logic for room slots                
                elif self.floor_plan[row, col] == self.room_char and color_plan[row, col] == 0:
                    
                    # Check if any of the adjacent room slots are already colorized
                    color = self.get_neighboring_colors(color_plan, row, col)

                    if not color:
                        # New Room!
                        current_color += 1
                        selected_color = current_color
                    else:
                        selected_color = color
                    
                    # Paint the 'slot'
                    color_plan[row, col] = selected_color

        return color_plan

    def print_color_floor_plan(self, color_strategy: ColorStrategy) -> bool:
        color_plan = self.color_floor_plan()

        for row in color_plan:
            for value in row:
                color = color_strategy.get_color(value)
                print(color, end="")
            print()

        return True

