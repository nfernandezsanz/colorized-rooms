import pytest
import numpy as np
from models import FloorPlan

@pytest.fixture
def expected_matrix():
    return np.array([
        ['#', '#', '#', '#', '#', '#', '#', '#', '#', '#'],
        ['#', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', '#'],
        ['#', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', '#'],
        ['#', '#', ' ', '#', '#', ' ', ' ', ' ', ' ', '#'],
        ['#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'],
        ['#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'],
        ['#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'],
        ['#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'],
        ['#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'],
        ['#', '#', '#', '#', '#', '#', '#', '#', '#', '#']
    ])
    
def test_read_floor_plan_existing_file(expected_matrix):
    # Test reading an existing floor plan file
    floor_plan = FloorPlan('tests/samples/square.txt')
    matrix = floor_plan.read_floor_plan()
    assert np.array_equal(matrix, expected_matrix)

def test_read_floor_plan_non_existing_file():
    # Test reading a non-existing floor plan file
    with pytest.raises(FileNotFoundError):
        floor_plan = FloorPlan('non_existing_file.txt')
        floor_plan.read_floor_plan()