import pytest
from models import FloorPlan

def test_wall_char():
    floor_plan = FloorPlan('tests/samples/square.txt', wall_char='#')
    assert floor_plan.wall_char == "#"
    
def test_room_char():
    floor_plan = FloorPlan('tests/samples/square.txt', room_char=" ")
    assert floor_plan.room_char == " "

def test_invalid_wall_char():
    floor_plan = FloorPlan('tests/samples/square.txt')
    with pytest.raises(ValueError, match="Wall character must be a single non-numeric character."):
        floor_plan.wall_char = "12"

def test_invalid_room_char():
    floor_plan = FloorPlan('tests/samples/square.txt')
    with pytest.raises(ValueError, match="Room character must be a single non-numeric character."):
        floor_plan.room_char = "0"
